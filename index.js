const express = require('express');
const bodyParser = require('body-parser');
const pretty = require('express-prettify');

const app = express();
const port = 3000;

app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);
app.use(pretty({ query: 'pretty' }));

app.get('/', (req, res) => {
  res.json({'message': 'Aplikasi Antrian'});
})

require("./app/routes/antrian.routes.js")(app);

app.listen(port, () => {
  console.log(`Aplikasi siap di http://localhost:${port}`)
});
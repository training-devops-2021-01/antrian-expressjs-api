const Antrian = require("../models/antrian.model.js");

exports.findAll = (req, res) => {
  Antrian.findAll((err, data) => {
    if (err)
    res.status(500).send({
      message:
        err.message || "Some error occurred while retrieving customers."
    });
    else res.json(data);
  });
};
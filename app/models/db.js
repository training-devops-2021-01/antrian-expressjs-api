const mysql = require('mysql');
const config = require('../config/database.js');

var connection = mysql.createPool(config.db);

module.exports = connection;
const sql = require("./db.js");

const Antrian = function(antrian) {
    this.email = antrian.email;
    this.waktu_pendaftaran = antrian.waktu_pendaftaran;
    this.urutan = antrian.urutan;
    this.no_hp = antrian.no_hp;
};

Antrian.findAll = result => {
    sql.query("select * from antrian order by urutan", (err, res) => {
        if(err){
            console.log("error : ",err);
            result(null, err);
            return;
        }

        console.log("antrian : ",res);
        result(null, res);
    })
};

module.exports = Antrian;
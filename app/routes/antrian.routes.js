module.exports = app => {
    const antrian = require("../controllers/antrian.controller.js");

    app.get("/antrian", antrian.findAll);
};
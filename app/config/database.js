const env = process.env;

const config = {
  db: { /* don't expose password or any sensitive info, done only for demo */
    host: env.DB_HOST || 'localhost',
    user: env.DB_USER || 'antrian',
    password: env.DB_PASSWORD || 'antrian4432',
    database: env.DB_NAME || 'antriandb',
  }
};

module.exports = config;
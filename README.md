# Aplikasi Antrian #

* Contoh membuat docker image dari aplikasi ExpressJS
* Contoh deployment dengan Docker Compose
* Contoh deployment dengan Kubernetes
* Otomasi CI/CD dengan Gitlab CI
* Versioning aplikasi dengan Semantic Versioning

## Model Scaling Aplikasi ##

[![Application Scaling](./img/application-scale-up-out.jpg)](./img/application-scale-up-out.jpg)

## Berbagai metode deployment ##

[![Deployment](./img/berbagai-mode-deployment.jpg)](./img/berbagai-mode-deployment.jpg)

## Skema Versioning ##

[![Versioning](./img/versioning.jpg)](./img/versioning.jpg)

## Konsep Build Script ##

[![Build Script](./img/build-script-content.jpg)](./img/build-script-content.jpg)

## Skema CI/CD ##

[![Skema CI/CD](./img/skema-ci-cd.jpg)](./img/skema-ci-cd.jpg)

# Menjalankan Aplikasi Antrian #

1. Jalankan MariaDB dengan Docker

    ```
    docker run  \
    --env MYSQL_ROOT_PASSWORD=root1234 \
    --env MYSQL_DATABASE=antriandb \
    --env MYSQL_USER=antrian \
    --env MYSQL_PASSWORD=antrian4432 \
    -v `pwd`/mysql-data:/var/lib/mysql \
    -p 3306:3306 \
    --name antriandb mariadb:10 
    ```

2. Membuat skema database dan insert sample data

    ```
    mysql -h 127.0.0.1 -u antrian -p antriandb app/sql/01-schema.sql
    Password:
    ```

3. Build docker image

    ```
    docker build -t endymuhardin/antrian-expressjs-api .
    ```

4. Jalankan aplikasinya

    ```
    docker run --env DB_HOST=192.168.100.5 -p 3001:3000 endymuhardin/antrian-expressjs-api
    ```

# Upload Image ke Docker Hub 

1. Login ke docker hub

    ```
    docker login
    ```

2. Upload image ke docker hub

    ```
    docker push --all-tags endymuhardin/antrian-expressjs-api
    ```

# Deploy ke Minikube #

1. Buat persistent volume

    ```
    kubectl apply -f https://gitlab.com/training-devops-2021-01/belajar-kubernetes/-/raw/master/deploy-minikube/01-persistent-volume.yml
    ```

2. Jalankan database server

    ```
    kubectl apply -f https://gitlab.com/training-devops-2021-01/belajar-kubernetes/-/raw/master/deploy-minikube/02-database-mariadb.yml
    ```

3. Deploy versi tertentu, misal `5e0f29df`

    ```
    sed -i "s/latest/5e0f29df/g" deployment.yml
    kubectl apply -f deployment.yml
    ```

# Otomasi Deployment dengan Gitlab-CI #

1. Buat service account di kubernetes cluster

    ```
    kubectl -n kube-system create serviceaccount gitlab
    ```

2. Berikan permission untuk mendeploy aplikasi ke kubernetes cluster

    ```
    kubectl create clusterrolebinding add-on-cluster-admin --clusterrole=cluster-admin --serviceaccount=kube-system:gitlab
    ```

3. Dapatkan nama token untuk service account tersebut

    ```
    TOKENNAME=`kubectl -n kube-system get serviceaccount/gitlab -o jsonpath='{.secrets[0].name}'`
    ```

4. Dapatkan nilai token untuk nama token yang didapatkan pada langkah di atas

    ```
    TOKENVALUE=`kubectl -n kube-system get secret $TOKENNAME -o jsonpath='{.data.token}'`
    ```

5. Token didapatkan dalam bentuk base64 yang harus didecode dulu

    ```
    echo $TOKENVALUE | base64 --decode -
    ```
    
6. Pasang variabel berikut Setting CI/CD Gitlab:

    * `K8S_TOKEN` : nilai token dari langkah sebelumnya
    * `PRODUCTION_SERVER` : dari kubeconfig di local
    * `CERTIFICATE_AUTHORITY_DATA` : dari kubeconfig di local